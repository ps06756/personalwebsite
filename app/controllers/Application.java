package controllers;

import play.*;
import play.mvc.*;

import views.html.*;

public class Application extends Controller {

    public Result index() {
    	String name = "Pratik Singhal";
    	String profession = "Full Stack Developer";
        return ok(index.render(name, profession));
    }

}
